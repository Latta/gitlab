# Read more about this feature here: https://docs.gitlab.com/ee/user/application_security/api_fuzzing/

# Configure the scanning tool through the environment variables.
# List of the variables: https://docs.gitlab.com/ee/user/application_security/api_fuzzing/#available-variables
# How to set: https://docs.gitlab.com/ee/ci/yaml/#variables

variables:
    FUZZAPI_PROFILE: Quick
    FUZZAPI_VERSION: "1"
    FUZZAPI_CONFIG: .gitlab-api-fuzzing.yml
    FUZZAPI_TIMEOUT: 30
    FUZZAPI_REPORT: gl-api-fuzzing-report.json
    FUZZAPI_REPORT_ASSET_PATH: assets
    #
    # Wait up to 5 minutes for API Fuzzer and target url to become
    # available (non 500 response to HTTP(s))
    FUZZAPI_SERVICE_START_TIMEOUT: "300"
    #
    SECURE_ANALYZERS_PREFIX: "registry.gitlab.com/gitlab-org/security-products/analyzers"
    FUZZAPI_IMAGE: ${SECURE_ANALYZERS_PREFIX}/api-fuzzing:${FUZZAPI_VERSION}
    #

apifuzzer_fuzz_unlicensed:
    stage: fuzz
    allow_failure: true
    rules:
        - if: '$GITLAB_FEATURES !~ /\bapi_fuzzing\b/ && $API_FUZZING_DISABLED == null'
        - when: never
    script:
        - |
            echo "Error: Your GitLab project is not licensed for API Fuzzing."
        - exit 1

apifuzzer_fuzz:
    stage: fuzz
    image: $FUZZAPI_IMAGE
    variables:
        FUZZAPI_PROJECT: $CI_PROJECT_PATH
        FUZZAPI_API: http://localhost:80
        FUZZAPI_NEW_REPORT: 1
        FUZZAPI_LOG_SCANNER: gl-apifuzzing-api-scanner.log
        TZ: America/Los_Angeles
    allow_failure: true
    rules:
        - if: $API_FUZZING_DISABLED
          when: never
        - if: $API_FUZZING_DISABLED_FOR_DEFAULT_BRANCH &&
                $CI_DEFAULT_BRANCH == $CI_COMMIT_REF_NAME
          when: never
        - if: $CI_COMMIT_BRANCH && $GITLAB_FEATURES =~ /\bapi_fuzzing\b/
    script:
        #
        # Validate options
        - |
            if [ "$FUZZAPI_HAR$FUZZAPI_OPENAPI$FUZZAPI_POSTMAN_COLLECTION" == "" ]; then \
                echo "Error: One of FUZZAPI_HAR, FUZZAPI_OPENAPI, or FUZZAPI_POSTMAN_COLLECTION must be provided."; \
                echo "See https://docs.gitlab.com/ee/user/application_security/api_fuzzing/ for information on how to configure API Fuzzing."; \
                exit 1; \
            fi
        #
        # Run user provided pre-script
        - sh -c "$FUZZAPI_PRE_SCRIPT"
        #
        # Make sure asset path exists
        - mkdir -p $FUZZAPI_REPORT_ASSET_PATH
        #
        # Start API Security background process
        - dotnet /peach/Peach.Web.dll &> $FUZZAPI_LOG_SCANNER &
        - APISEC_PID=$!
        #
        # Start scanning
        - worker-entry
        #
        # Run user provided post-script
        - sh -c "$FUZZAPI_POST_SCRIPT"
        #
        # Shutdown API Security
        - kill $APISEC_PID
        - wait $APISEC_PID
        #
    artifacts:
        when: always
        paths:
            - $FUZZAPI_REPORT_ASSET_PATH
            - $FUZZAPI_REPORT
            - $FUZZAPI_LOG_SCANNER
        reports:
            api_fuzzing: $FUZZAPI_REPORT

# end
